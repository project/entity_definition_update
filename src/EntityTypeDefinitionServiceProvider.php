<?php declare(strict_types=1);

namespace Drupal\entity_definition_update;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\Finder\Finder;


class EntityTypeDefinitionServiceProvider extends ServiceProviderBase {

  public function register(ContainerBuilder $container) {
    $containerModules = $container->getParameter('container.modules');
    $finder = new Finder();

    $foldersWithServiceContainers = [];

    $foldersWithServiceContainers['Drupal\entity_definition_update\\'] = $finder->in(dirname($containerModules['entity_definition_update']['pathname']) . '/src/')->files()->name('*.php');

    $this->setClassDefinitionsInContainer($foldersWithServiceContainers, $finder, $container);
  }

  private function setClassDefinitionsInContainer(array $foldersWithServiceContainers, Finder $finder, ContainerBuilder $container): void {
    foreach ($foldersWithServiceContainers as $namespace => $files) {
      foreach ($finder as $fileInfo) {
        $class = $namespace
          . substr($fileInfo->getFilename(), 0, -4);
        // don't override any existing service
        if ($container->hasDefinition($class)) {
          continue;
        }
        $definition = new Definition($class);
        $definition->setAutowired(TRUE);
        $container->setDefinition($class, $definition);
      }
    }
  }

}
